#Creater tf object
# data "azurerm_resource_group" "diegomez" {
#   name = "diego-gomez"
# }


# resource "azurerm_virtual_network" "test-network" {
#   name                = "dg-09-network"
#   address_space       = ["10.0.0.0/16"]
#   location            = data.azurerm_resource_group.diegomez.location
#   resource_group_name = data.azurerm_resource_group.diegomez.name
# }

#aws code

# resource "aws_vpc" "main" {
#   cidr_block       = "10.0.0.0/16"
#   tags = {
#     Name = "dg-vpc"
#   }
# }

# Create a Security group
# resource "aws_security_group" "dg-tf-sg" {
#   name        = "dg-tf-sg"
#   description = "Tf security group demo"
#   vpc_id      = aws_vpc.main.id

#   ingress {
#     description = "SSH ALLOWED"
#     from_port   = 22
#     to_port     = 22
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   ingress {
#     description = "TCP ALLOWED"
#     from_port   = 8080
#     to_port     = 8080
#     protocol    = "tcp"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#     ipv6_cidr_blocks = ["::/0"]
#   }

#   tags = {
#     Name = "dg-tf-sg"
#   }
# }
# VPC
resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" = "dg-09-vpc"
  }
}
# Public Subnet
resource "aws_subnet" "public" {
  vpc_id = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"
  map_public_ip_on_launch = true
  tags = {
    "Name" = "dg-09-public-subnet"
  }
}
# Internet Gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id
  tags = {
    "Name" = "dg-09-igw"
  }
}
# Public Route Table
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    "Name" = "dg-09-prt"
  }
}
# Association between public subnet and public route table
resource "aws_route_table_association" "public" {
  subnet_id = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}
# Creating Security Group
resource "aws_security_group" "scg" {
  name = "dg-09-scg"
  vpc_id = aws_vpc.main.id
  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 22
    protocol = "tcp"
    to_port = 22
  }
  ingress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
  }
  egress {
    cidr_blocks = [ "0.0.0.0/0" ]
    from_port = 0
    ipv6_cidr_blocks = [ "::/0" ]
    protocol = "-1"
    to_port = 0
  }
  tags = {
    "Name" = "dg-09-sch"
  }
}
# Creating EC2 Instance
resource "aws_instance" "foo" {
  ami = "ami-0d5eff06f840b45e9"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ aws_security_group.scg.id ]
  subnet_id = aws_subnet.public.id
}